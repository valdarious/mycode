# mycode (Project Title)

Learning Gitlab for the purpose of work to better understand AWX along side of Ansible.
I want to learn how to version control projects with git.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites

What things are needed to install the software and how to install them. For now, maybe copy in:
"How to Install the Language: "

## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* **Your Name** - *Initial work* - [YourWebsite](https://example.com/)
